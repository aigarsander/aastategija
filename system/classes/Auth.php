<?php namespace Halo;


/**
 * Class auth authenticates user and permits to check if the user has been logged in
 * Automatically loaded when the controller has $requires_auth property.
 */
class Auth
{

    public $logged_in = FALSE;

    function __construct()
    {
        if (isset($_SESSION['user_id'])) {
            $this->logged_in = TRUE;
            $user = get_first("SELECT *
                               FROM users
                               WHERE user_id = '{$_SESSION['user_id']}'");
            $this->load_user_data($user);

        }
    }

    /**
     * Dynamically add all user table fields as object properties to auth object
     * @param $user
     */
    public
    function load_user_data($user)
    {


        foreach ($user as $user_attr => $value) {
            $this->$user_attr = $value;
        }
        $this->logged_in = TRUE;
    }

    /**
     * Verifies if the user is logged in and authenticates if not and POST contains username, else displays the login form
     * @return bool Returns true when the user has been logged in
     */
    function require_auth()
    {
        global $db;


        // If user has already logged in...
        if ($this->logged_in) {
            return TRUE;
        }


        // Not all credentials were provided
        if (!(isset($_POST['fname']) || isset($_POST['lname']) || isset($_POST['isik']))) {

            $this->show_login();

        }
        $errors = array();

        if(empty($_POST['fname'])) {
            $msg = 'Sisestage eesnimi!';
            $errors[] = $msg;
            $this->show_login($errors);
        } else if(is_numeric($_POST['fname'])){
            $msg = 'Eesnimi ei tohi sisaldada numbreid!';
            $errors[] = $msg;
            $this->show_login($errors);
        } else if(preg_match('/[^a-z\s-]/i', $_POST['fname'])){
            $msg = 'Eesnimi ei tohi sisaldada sümboleid(va. -)!';
            $errors[] = $msg;
            $this->show_login($errors);
        }

        if(empty($_POST['lname'])) {
            $msg = 'Sisestage perekonna nimi!';
            $errors[] = $msg;
            $this->show_login($errors);
        } else if(is_numeric($_POST['lname'])){
            $msg = 'Perekonna nimi ei tohi sisaldada numbreid!';
            $errors[] = $msg;
            $this->show_login($errors);
        } else if(preg_match('/[^a-z\s-]/i', $_POST['lname'])){
            $msg = 'Perekonna nimi ei tohi sisaldada sümboleid(va. -)!';
            $errors[] = $msg;
            $this->show_login($errors);
        }

        if(empty($_POST['isik'])) {
            $msg = 'Sisestage isikukood!';
            $errors[] = $msg;
            $this->show_login($errors);
        } else if(!is_numeric($_POST['isik'])) {
            $msg = 'Isikukood ei tohi sisaldada tähti.';
            $errors[] = $msg;
            $this->show_login($errors);
        } else if(strlen($_POST['isik']) != 11) {
            $msg = 'Isikukood ei ole õige pikkusega.';
            $errors[] = $msg;
            $this->show_login($errors);
        } else if(!preg_match('/^[0-9]*$/', $_POST['isik'])) {
            $msg = 'Isikukood ei tohi sisaldada sümboleida.';
            $errors[] = $msg;
            $this->show_login($errors);
        }

        // Prevent SQL injection
        $isk = mysqli_escape_string($db, $_POST['isik']);
        $en = mysqli_escape_string($db, $_POST['fname']);
        $pn = mysqli_escape_string($db, $_POST['lname']);

        $user = get_first('SELECT * FROM users WHERE isikukood ='.$isk);

        if(isset($user['user_id']) && $user['is_admin'] == 0 && $user['can_attend'] === '1') {
            $msg = 'Teil pole õigust testi teha!';
            $errors[] = $msg;
            $this->show_login($errors);
        }

        if (!isset($user['user_id'])) {
            insert('users', array('eesnimi' => $en, 'perenimi' => $pn, 'isikukood' => $isk));
        }

        // User has provided correct login data if we are here
        $user = get_first('SELECT * FROM users WHERE isikukood ='.$isk);

        if($en != $user['eesnimi'] || $pn != $user['perenimi']) {
            $msg = 'Sisestatud andmed ei ühti andmebaasiga!.';
            $errors[] = $msg;
            $this->show_login($errors);
        }

        $_SESSION['user_id'] = $user['user_id'];


        // Load $this->auth with users table's field values
        $this->load_user_data($user);


        return true;

    }

    /**
     * @param $errors
     */
    protected function show_login($errors = null)
    {
        // Display the login form
        require 'templates/auth_template.php';
        // Prevent loading the requested controller (not authenticated)
        exit();
    }
}
