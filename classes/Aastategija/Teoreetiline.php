<?php
namespace Aastategija;


class Teoreetiline{
    static function AssignQuestions($questions) {

        //create a string of right answers and assign it to a session variable
        $r_string = '';
        foreach ($questions as $question) {
            $r_string .= (string)$question['right_answ'].'|';
        }

        $_SESSION['r_str'] = (string)$r_string;
    }

    static function SaveResults($answers) {
        // create variables
        $punktid = 0;
        $a_string = '';

        // creates a string for answered values for later to be exploded and checked
        for($i = 0; $i < 10; $i++) {
            $a_string .= (string)$answers[$i].'|';
            $_SESSION['a_str'] = (string)$a_string;
        }

        // calculates the total points user received from the correct answers
        $r = explode('|', $_SESSION['r_str']);
        $a = explode('|', $a_string);
        for($j = 0; $j < 10; $j++) {
            if($r[$j] == $a[$j]) {
                $punktid++;
            }
        }

        // creates a sql
        $sql = array(
            'quiz_result' => $punktid,
            'exercise_result' => ''
        );

        // returns an update function
        return update('users', $sql, 'user_id ='.$_SESSION['user_id']);
    }
}