<?php
namespace Aastategija;


class Questions {
    static function AddQuestion() {
        // add a question to the database via admin panel
        $tbl_name = 'questions';
        $temp = array(
            'question' => '',
            'answ1' => '',
            'answ2' => '',
            'answ3' => '',
            'right_answ' => ''
        );

        insert($tbl_name, $temp);

        //return to admin's view over questions
        header('Location:'.BASE_URL.'admin/questions');
    }

    static function EditQuestion($q_id, $q, $a1, $a2, $a3, $ra) {
        //edit selected question and update database
        $tbl_name = 'questions';
        $tbl_where = 'question_id ='.$q_id;
        $temp = array(
            'question' => $q,
            'answ1' => $a1,
            'answ2' => $a2,
            'answ3' => $a3,
            'right_answ' => $ra
        );

        update($tbl_name, $temp, $tbl_where);

        //return to admin's view over questions
        header('Location:'.BASE_URL.'admin/questions');
    }

    static function RemoveQuestion($q_id) {
        //delete specific row from database
        $sql = 'DELETE FROM questions WHERE question_id='.$q_id;
        q($sql);

        //return to admin's view over questions
        header('Location:'.BASE_URL.'admin/questions');
    }
}