<?php
namespace Aastategija;


class Praktiline{
    static function UpdatePractical($p_id) {
        // Updates user's practical to currently assigned exercise
        $user_temp = array(
            'assigned_prac' => $p_id
        );

        update('users', $user_temp, 'user_id =' . $_SESSION['user_id']);
    }

    static function SaveData($data) {
        // Saves user input to database
        $user_temp = array(
            'exercise_input' => $data,
            'can_attend' => '1'
        );

        return update('users', $user_temp, 'user_id =' . $_SESSION['user_id']);
    }
}