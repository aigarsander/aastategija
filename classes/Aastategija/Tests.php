<?php
namespace Aastategija;


class Tests{
    static function Permit($u_id) {
        // gives permission to redo the test

        $user_temp = array(
            'can_attend' => '0'
        );

        update('users', $user_temp, 'user_id ='.$u_id);
        header('Location:'.BASE_URL.'admin/marking');
    }

    static function Hinda($t_points, $p_points) {
        //Check if the practical exercise points are bigger than 0 but not bigger than 10
        if ($p_points < 0) {
            $p_points = 0;
        }

        if ($p_points > 10) {
            $p_points = 10;
        }
        $pk = $t_points + $p_points;

        // return results as an array
        $user_temp = array(
            'exercise_result' => $p_points,
            'hinnatud' => '1',
            'kokku' => $pk
        );

        return $user_temp;
    }

    static function Reevaluate($u_id) {

        //Assign new grade to the user's database
        $user_temp = array(
            'hinnatud' => '0'
        );

        update('users', $user_temp, 'user_id ='.$u_id);
        header('Location:'.BASE_URL.'admin/hinda/'.$u_id);
    }
}