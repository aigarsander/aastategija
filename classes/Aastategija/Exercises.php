<?php
namespace Aastategija;


class Exercises{
    static function AddExercise() {
        // Assigns default values then inserts a new row into database
        $temp = array(
            'prac_id' => '',
            'exercise' => ''
        );

        insert('practical', $temp);
        header('Location:'.BASE_URL.'admin/exercises');
    }

    static function EditExercise($p_id, $prac) {
        // Assigns new data then updates the database
        $tbl_where = 'prac_id ='.$p_id;
        $temp = array(
            'exercise' => $prac
        );

        update('practical', $temp, $tbl_where);
        header('Location:'.BASE_URL.'admin/exercises');
    }

    static function RemoveExercise($p_id) {
        // Removes data from database based on id
        $sql = 'DELETE FROM practical WHERE prac_id='.$p_id;
        q($sql);
        header('Location:'.BASE_URL.'admin/exercises');
    }
}