<?php
namespace Halo;
use Aastategija\Tests as T;
use Aastategija\Exercises as E;
use Aastategija\Questions as Q;

class admin extends Controller {
    function index() {

        // checks if sessions user is a administrator
        $_SESSION['is_admin'] = 1;
    }

    function questions() {
        // Assign data to page
        $this->questions = get_all("SELECT * FROM questions");
    }

    function addQuestion() {

        // call a method to save question data
        Q::AddQuestion();
    }

    function editQuestion() {

        // Assign data to page based on input
        $question_id = $this->params[0];
        $this->question = get_first('SELECT * FROM questions WHERE question_id = '.$question_id);

        // call a method after check to update database
        if(isset($_POST['save'])) {
            Q::EditQuestion($question_id, $_POST['que'], $_POST['answ1'], $_POST['answ2'], $_POST['answ3'],$_POST['right_answ']);
        }
    }

    function removeQuestion() {

        // remove a database question via admin panel
        $question_id = $this->params[0];
        $this->question = get_first('SELECT * FROM questions WHERE question_id = '.$question_id);

        // call a method to remove selected question
        Q::RemoveQuestion($question_id);
    }

    function exercises() {

        // Assign data to page
        $this->exercises = get_all("SELECT * FROM practical");
    }

    function addExercises() {

        // call a method to add a new exercise
        E::AddExercise();
    }

    function editExercises() {

        // Assign data to page based on input
        $prac_id = $this->params[0];
        $this->exercise = get_first('SELECT * FROM practical WHERE prac_id = '.$prac_id);

        if(isset($_POST['save'])) {

            // call a method to save edited data
            E::EditExercise($prac_id, $_POST['prac']);
        }
    }

    function removeExercises() {

        // Assign data to page based on input
        $prac_id = $this->params[0];
        $this->exercise = get_first('SELECT * FROM practical WHERE prac_id = '.$prac_id);

        // call a method to remove selected practical test
        E::RemoveExercise($prac_id);
    }

    function marking() {

        // Assign data to page
        $this->users = get_all("SELECT * FROM users WHERE assigned_prac IS NOT NULL AND hinnatud = 0");
    }

    function hinda() {

        // Assign data to page based on input
        $u_id = $this->params[0];
        $user = get_first('SELECT * FROM users WHERE user_id ='.$u_id);
        $this->user = $user;
        $this->prac = get_first("SELECT * FROM practical WHERE prac_id=".$user['assigned_prac']);
        $this->tp = (int)$user['quiz_result'];

        if(isset($_POST['salvesta'])){

            // call a method to assign data, then update the database and return to ajax with a response
            exit(update('users', T::Hinda($_POST['phinne'], $_POST['hinne']), 'user_id='.$user['user_id']) ? 'Ok' : 'Fail');
        }
    }

    function results() {

        // Assign data to page
        $this->users = get_all("SELECT * FROM users WHERE hinnatud = 1 ORDER BY kokku DESC");
    }

    function luba() {

        // Assign data to page based on input
        $u_id = $this->params[0];
        $this->user = get_first('SELECT * FROM users WHERE user_id ='.$u_id);

        // call a method to permit the user for another test attempt
        T::Permit($u_id);
    }

    function paranda() {

        // Assign data to page based on input
        $u_id = $this->params[0];
        $this->user = get_first('SELECT * FROM users WHERE user_id ='.$u_id);

        // call a method to re-evaluate selected user's practical grade
        T::Reevaluate($u_id);
    }
}