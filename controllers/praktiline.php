<?php
namespace Halo;

use Aastategija\Praktiline as P;

class praktiline extends Controller {

    function index(){

        // Assign data to page based on input
        $practical = get_first('SELECT * FROM practical ORDER BY RAND() LIMIT 1');
        $this->practical = $practical;

        //Denies reloading the page for new answers.
        if(isset($_SESSION['firsttimep'])) {
            if(!isset($_SESSION['saving'])) {
                header('Location:' . BASE_URL . 'logout');
            }
        } else {
            $_SESSION['firsttimep'] = true;
        }

        // call a method to update users practical to currently assigned one
        P::UpdatePractical($practical['prac_id']);
    }

    function results() {
        // Assign data to page
        $this->user = get_first('SELECT * FROM users where user_id ='.$_SESSION['user_id']);
    }


    function salvesta() {

        //if data is being sent then call a method to save it and redirect
        if(isset($_POST['data'])) {
            exit(P::SaveData($_POST['data']) ? 'Ok' : 'Fail');
        }
    }
}