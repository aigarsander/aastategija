<?php namespace Halo;

use Aastategija\Teoreetiline as T;

class teoreetiline extends Controller{
    function index() {
        //nothing special happens here
    }

    function test() {
        // Assign data to page
        $questions = get_all("SELECT * FROM questions ORDER BY RAND() LIMIT 10");
        $this->questions = $questions;

        //Denies reloading the page for new answers.
        if(isset($_SESSION['firsttime'])) {
            if(!isset($_SESSION['saving'])) {
                header('Location:' . BASE_URL . 'logout');
            }
        } else {
            $_SESSION['firsttime'] = true;
        }

        // call a method to save assigned questions
        T::AssignQuestions($questions);
    }

    function salvesta() {

        // create an array of answers then send it to a method for processing
        if(isset($_POST['salvesta'])) {
            //Check if user answered to all questions otherwise alert the user
            for($i = 1; $i < 10; $i++) {
                if(isset($_POST['que'.$i])) {
                    continue;
                } else {
                    unset($_POST['salvesta']);
                    exit('ans');
                }
            }

            //create an array to use it in a method
            $answers = array(
                $_POST['que1'], $_POST['que2'], $_POST['que3'], $_POST['que4'], $_POST['que5'],
                $_POST['que6'], $_POST['que7'], $_POST['que8'], $_POST['que9'], $_POST['que10']
            );

            exit(T::SaveResults($answers) ? 'Ok' : 'Fail');
        }
    }
}