<head>
    <style>
        hr {
            height: 1px;
            background-color:#555;
            margin-top: 5px;
            margin-bottom: 5px;
            width: 100%;
        }

        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            /* display: none; <- Crashes Chrome on hover */
            -webkit-appearance: none;
            margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
        }
    </style>
</head>
<ul class="nav nav-pills nav-pills">
    <li role="presentation"><a href="#">Pealeht</a></li>
    <li role="presentation"><a href="admin/<?='questions'?>">Teoreetiline</a></li>
    <li role="presentation"><a href="admin/<?='exercises'?>">Praktiline</a></li>
    <li role="presentation" class="active"><a href="admin/<?='marking'?>">Hindamine</a></li>
    <li role="presentation"><a href="admin/<?='results'?>">Tulemused</a></li>
</ul><hr>
<div class="col-lg-12"><b>Nimi:</b> <?=$user['eesnimi'].' '.$user['perenimi']?></div>
<hr>
<div class="col-lg-12"><b>Teoreetiline test:</b> <?=$user['quiz_result']?> punkti</div>
<hr>
<div class="col-lg-6"><b>Praktiline Ülesanne</b></div><div class="col-lg-6"><b>Praktiline Lahendus(KOOD)</b></div>
<div class="col-lg-6"><?=$prac['exercise']?></div><div class="col-lg-6"><?=htmlspecialchars($user['exercise_input'])?></div><hr>
<div class="col-lg-12"><b>Praktiline Lahendus(EELVAADE)</b></div>
<div class="col-lg-12"><?=html_entity_decode($user['exercise_input'])?></div>
<hr>
<b>Hinne:</b>   <input type="number" name="hinne" id="hinne"><hr>
<form action="<?=BASE_URL?>" method="post" id="hinda">
    <input type="submit" class="btn btn-success" name="save" value="HINDA">
</form>

<script>
    $(function () {
        $('#hinda').bind('submit', function (event) {
            $(window).off('beforeunload');
            event.preventDefault();
            $u = $(this).attr('action');
            var $salvesta = ['salvesta=1'];
            var ph = <?php echo $user['quiz_result']; ?>;
            var h = $('#hinne').val();
            var uid = <?php echo $user['user_id']; ?>;
            //alert($salvesta + '&hinne=' + h + '&phinne=' + ph + '&uid=' + uid);
            $.ajax({
                type: 'POST',
                data: $salvesta + '&hinne=' + h + '&phinne=' + ph + '&uid=' + uid,
                success: function (response) {
                    if(response == 'Ok') {
                        window.location = $u + 'admin/marking';
                    } else {
                        alert('Tekkis tõrge!');
                    }
                }
            });
        });
    });
</script>
