<ul class="nav nav-pills nav-pills">
    <li role="presentation"><a href="#">Pealeht</a></li>
    <li role="presentation" class="active"><a href="admin/<?='questions'?>">Teoreetiline</a></li>
    <li role="presentation"><a href="admin/<?='exercises'?>">Praktiline</a></li>
    <li role="presentation"><a href="admin/<?='marking'?>">Hindamine</a></li>
    <li role="presentation"><a href="admin/<?='results'?>">Tulemused</a></li>
</ul><hr>

<div class="row">
    <div class="col-lg-3">Küsimus</div>
    <div class="col-lg-2">Vastus #1</div>
    <div class="col-lg-2">Vastus #2</div>
    <div class="col-lg-2">Vastus #3</div>
    <div class="col-lg-2">Õige Vastus</div>
</div><hr>

<?php foreach ($questions as $question): ?>
    <div class="row">
        <div class="col-lg-3"><?=htmlspecialchars($question['question']) ?></div>
        <div class="col-lg-2"><?=htmlspecialchars($question['answ1']) ?></div>
        <div class="col-lg-2"><?=htmlspecialchars($question['answ2']) ?></div>
        <div class="col-lg-2"><?=htmlspecialchars($question['answ3']) ?></div>
        <div class="col-lg-1"><?=htmlspecialchars($question['right_answ']) ?></div>
        <a class="col-lg-1 btn btn-success" href="<?=BASE_URL.'admin/editQuestion/'.$question['question_id']?>">EDIT</a>
        <a class="col-lg-0.5 btn btn-danger" href="<?=BASE_URL.'admin/removeQuestion/'.$question['question_id']?>">X</a>
    </div><hr>
<?php endforeach ?>

<div class="row">
    <div class="col-lg-5"></div>
    <a class="col-lg-2 btn btn-warning" href="<?=BASE_URL.'admin/addQuestion'?>">ADD ROW</a>
    <div class="col-lg-5"></div>
</div><br><br><br>

