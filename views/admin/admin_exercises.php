<ul class="nav nav-pills nav-pills">
    <li role="presentation"><a href="#">Pealeht</a></li>
    <li role="presentation"><a href="admin/<?='questions'?>">Teoreetiline</a></li>
    <li role="presentation" class="active"><a href="admin/<?='exercises'?>">Praktiline</a></li>
    <li role="presentation"><a href="admin/<?='marking'?>">Hindamine</a></li>
    <li role="presentation"><a href="admin/<?='results'?>">Tulemused</a></li>
</ul><hr><hr>

<div class="row">
    <div class="col-lg-12">Harjutus</div>
</div><hr>

<?php foreach ($exercises as $exercise): ?>
    <div class="row">
        <div class="col-lg-10"><?=htmlspecialchars($exercise['exercise']) ?></div>
        <a class="col-lg-1 btn btn-success" href="<?=BASE_URL.'admin/editExercises/'.$exercise['prac_id']?>">EDIT</a>
        <a class="col-lg-1 btn btn-danger" href="<?=BASE_URL.'admin/removeExercises/'.$exercise['prac_id']?>">X</a>
    </div><hr>
<?php endforeach ?>

<div class="row">
    <div class="col-lg-5"></div>
    <a class="col-lg-2 btn btn-warning" href="<?=BASE_URL.'admin/addExercises'?>">ADD ROW</a>
    <div class="col-lg-5"></div>
</div><br><br><br>

