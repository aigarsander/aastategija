<ul class="nav nav-pills nav-pills">
    <li role="presentation"><a href="#">Pealeht</a></li>
    <li role="presentation" class="active"><a href="admin/<?='questions'?>">Teoreetiline</a></li>
    <li role="presentation"><a href="admin/<?='exercises'?>">Praktiline</a></li>
    <li role="presentation"><a href="admin/<?='marking'?>">Hindamine</a></li>
    <li role="presentation"><a href="admin/<?='results'?>">Tulemused</a></li>
</ul>

<form action="" method="post" role="form">
    <label>Küsimus</label>
    <input type="text" class="form-control" name="que" placeholder="<?=htmlspecialchars($question['question'])?>">
    <label>Vastus #1</label>
    <input type="text" class="form-control" name="answ1" placeholder="<?=htmlspecialchars($question['answ1'])?>">
    <label>Vastus #2</label>
    <input type="text" class="form-control" name="answ2" placeholder="<?=htmlspecialchars($question['answ2'])?>">
    <label>Vastus #3</label>
    <input type="text" class="form-control" name="answ3" placeholder="<?=htmlspecialchars($question['answ3'])?>">
    <label>Õige Vastus</label>
    <select class="form-control" name="r_answ">
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
    </select>

    <a class="col-lg-1 btn btn-danger" href="admin/<?='questions'?>">BACK</a>
    <button type="submit" class="col-lg-1 btn btn-success" name="save">SAVE</button>
</form>