<ul class="nav nav-pills nav-pills">
    <li role="presentation"><a href="#">Pealeht</a></li>
    <li role="presentation"><a href="admin/<?='questions'?>">Teoreetiline</a></li>
    <li role="presentation" class="active"><a href="admin/<?='exercises'?>">Praktiline</a></li>
    <li role="presentation"><a href="admin/<?='marking'?>">Hindamine</a></li>
    <li role="presentation"><a href="admin/<?='results'?>">Tulemused</a></li>
</ul>

<form action="" method="post" role="form">
    <label>Harjutus</label>
    <textarea rows="10" class="form-control col-lg-12 " name="prac" placeholder="<?=htmlspecialchars($exercise['exercise'])?>"></textarea>

    <div class="col-lg-10"></div>
    <a class="col-lg-1 btn btn-danger" href="admin/<?='exercises'?>">BACK</a>
    <button type="submit" class="col-lg-1 btn btn-success" name="save">SAVE</button>
</form>