<ul class="nav nav-pills nav-pills">
    <li role="presentation"><a href="#">Pealeht</a></li>
    <li role="presentation"><a href="admin/<?='questions'?>">Teoreetiline</a></li>
    <li role="presentation"><a href="admin/<?='exercises'?>">Praktiline</a></li>
    <li role="presentation"><a href="admin/<?='marking'?>">Hindamine</a></li>
    <li role="presentation" class="active"><a href="admin/<?='results'?>">Tulemused</a></li>
</ul><hr>

<div class="row">
    <div class="col-lg-2">Eesnimi</div>
    <div class="col-lg-2">Perekonna nimi</div>
    <div class="col-lg-2">Teoreetiline tulemus</div>
    <div class="col-lg-2">Praktiline tulemus</div>
    <div class="col-lg-2">Punktid kokku</div>
</div><hr>

<?php foreach ($users as $user): ?>
    <div class="row">
        <div class="col-lg-2"><?=htmlspecialchars($user['eesnimi']) ?></div>
        <div class="col-lg-2"><?=htmlspecialchars($user['perenimi']) ?></div>
        <div class="col-lg-2"><?=htmlspecialchars($user['quiz_result']) ?></div>
        <div class="col-lg-2"><?php
            if($user['exercise_result'] === (string)-1) {
                echo 'HINDAMATA';
            } else {
                echo htmlspecialchars($user['exercise_result']);
            }
            ?></div>
        <div class="col-lg-2"><?=htmlspecialchars($user['kokku']) ?></div>
        <a href="admin/paranda/<?=$user['user_id']?>" class="btn btn-success col-lg-1">PARANDA</a>
    </div><hr>
<?php endforeach ?>