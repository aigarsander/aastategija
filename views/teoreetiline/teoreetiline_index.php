<div class="row">
    <h1 class="col-lg-12" style="text-align:center;">Teoreetiline test</h1>
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <div style="text-align: left; margin: 40px 40px 50px 110px; font-weight: bold">
        <p>Teretulemast Tartu Kutsehariduskeskuse noorem tarkvaraarendaja (veebispetsialist) sisseastumis testile.</p>
        <p>Järgnev test koosneb kahest osast, teoreetilisest ja praktilisest.</p>
        <p>Teoreetilises osas on kümme küsimust valikvastustega ja praktilises osas tuleb lahendada üks ülesanne.</p>
        <p>Teoreetilise osa punktid saate teada hiljem, kui komisjon vaatab teie töö üle.</p>
        </div>
    </div><br><br>

    <div class="col-lg-2"></div>
        <div class="row">
            <div class="col-lg-5"></div>
            <a class="col-lg-2 btn btn-primary" href=<?=BASE_URL.'teoreetiline/test'?>>ALUSTA</a>
            <div class="col-lg-5"></div>
        </div><br>
</div>