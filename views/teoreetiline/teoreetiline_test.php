<script src="/vendor/components/jquery/jquery.js"></script>

<?php $ind = 1; ?>
    <form action="<?=BASE_URL?>" method="post" id="quests">
        <?php foreach($questions as $question) {?>
        <legend>#<?=$ind?><br><?=htmlspecialchars($question['question'])?></legend>
        <input type="radio" class="col-lg-0.5" name="que<?=$ind?>" value="1"><?=htmlspecialchars($question['answ1'])?><br>
        <input type="radio" class="col-lg-0.5" name="que<?=$ind?>" value="2"><?=htmlspecialchars($question['answ2'])?><br>
        <input type="radio" class="col-lg-0.5" name="que<?=$ind?>" value="3"><?=htmlspecialchars($question['answ3'])?><br><br><br><br>
        <?php
        $ind ++;
        } ?>
        <input type="submit" class="btn btn-success col-lg-2" value="Edasi"><br><br><br><br>
</form><br><br><br><br>

<script>
    $(window).bind('beforeunload', function(){
        return 'Värskendades lehekülge, logitakse teid automaatselt välja ning testi enam sooritada ei saa!';
    });

    $(function () {
        $('#quests').bind('submit', function (event) {
            $(window).off('beforeunload');
            event.preventDefault();
            var $salvesta = ['salvesta=1'];
            $u = $(this).attr('action');
            $.ajax({
                type: 'POST',
                url: $u + 'teoreetiline/salvesta',
                data: $salvesta + "&" + $(this).serialize(),
                success: function (result) {
                    alert(this.data);
                    if(result == 'Ok') {
                        window.location = $u + 'praktiline';
                    }

                    if(result == 'ans') {
                        alert('Palun Vastake kõikidele küsimustele!');
                    }
                }
            });
        });
    });
</script>