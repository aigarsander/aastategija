<style>
    #canvas {
        background-color: lightgray;
    }

    #dynamicframe {
        min-height: 200px;
        max-height: 500px;
    }

</style>

<h1"><b>Ülesanne</b></h1>
<hr>
<p><?=$practical['exercise']?></p>

<textarea id="canvas" class="col-lg-12" name="ta" rows="10" cols="140"></textarea>
<iframe name="dynamicframe" class="col-lg-12" id="dynamicframe" src="javascript:'-'"></iframe>
<form action="<?=BASE_URL?>" class="col-lg-10" id="finish">
    <input type="submit" class="btn btn-success col-lg-2" value="EDASI"/><br><br><br><br>
</form>

<script type="text/javascript">
    $('#canvas').on('change keyup paste', function() {
        $('#dynamicframe').contents().find('body').html($('#canvas').val());
    });

    $(window).bind('beforeunload', function(){
        return 'Värskendades lehekülge, logitakse teid automaatselt välja ning testi enam sooritada ei saa!';
    });

    $(function () {
        $('#finish').bind('submit', function (event) {
            $(window).off('beforeunload');
            event.preventDefault();
            $u = $(this).attr('action');
            var $salvesta = ['salvesta=1'];
            var textarea = $('#canvas').val();
            $.ajax({
                type: 'POST',
                url: $u + 'praktiline/salvesta',
                data: $salvesta + "&" + 'data=' + textarea,
                success: function (result) {
                    if(result == 'Ok') {
                        window.location = $u + 'praktiline/results';
                    }
                }
            });
        });
    });
</script>
